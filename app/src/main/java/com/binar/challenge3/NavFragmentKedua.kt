package com.binar.challenge3

import android.os.Bundle
import android.text.Layout
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.findNavController
import com.binar.challenge3.databinding.FragmentNavKeduaBinding

class NavFragmentKedua : Fragment() {

    private var _binding: FragmentNavKeduaBinding? = null
    private val binding get() = _binding!!
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentNavKeduaBinding.inflate(inflater, container, false)
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Mendapatkan data argument sesuai dengan key yang dimaksud
//        val aName = arguments?.getString(NavFragmentPertama.EXTRA_NAME)
//        binding.tvNama.text = "Nama kamu: $aName"

        binding.btnToFragmentKetiga.setOnClickListener { view ->
            if (binding.etName.text.isNullOrEmpty()) {
                Toast.makeText(requireContext(), "Kolom Nama masih kosong", Toast.LENGTH_SHORT).show()
            } else {
                val actionToFragmentKetiga = NavFragmentKeduaDirections.actionNavFragmentKeduaToNavFragmentKetiga()
                actionToFragmentKetiga.name = binding.etName.text.toString()
                view.findNavController().navigate(actionToFragmentKetiga)
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}