package com.binar.challenge3

import android.os.Bundle
import android.app.Person
import android.os.Build
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.binar.activity.R
import com.binar.activity.databinding.FragmentNavKetigaBinding

class NavFragmentKetiga : Fragment() {

    private var _binding: FragmentNavKetigaBinding? = null
    private val binding get() = _binding!!
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentNavKetigaBinding.inflate(inflater, container, false)
        return binding.root
    }

    @RequiresApi(Build.VERSION_CODES.P)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val aName = NavFragmentKetigaArgs.fromBundle(arguments as Bundle).name
        binding.tvName.text = "Nama: $aName"

        findNavController().currentBackStackEntry?.savedStateHandle.getLiveData<Person>(NavFragmentKeempat.EXTRA_DETAIL)
            ?.observe(viewLifecycleOwner) { detail ->

            binding.textusia.visibility = View.VISIBLE
            binding.textusia.text = "Usia : ${detail.umur}, ${cekUmur(detail.umur!!)}"

            binding.textalamat.visibility = View.VISIBLE
            binding.textalamat.text = detail.alamat

            binding.textpekerjaan.visibility = View.VISIBLE
            binding.textpekerjaan.text = detail.pekerjaan
        }

        binding.btnToFragmentKeempat.setOnClickListener { view ->
            val action = NavFragmentKetigaDirections.actionNavFragmentKetigaToNavFragmentKeempat()
            view.findNavController().navigate(action)
        }
    }
    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    fun cekUmur(input:Int): String{
        if (input!! % 2 == 0) return "bilangan genap"
        else return "bilangan ganjil"
    }
}