package com.binar.challenge3

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.binar.challenge3.databinding.FragmentNavKeempatBinding

class NavFragmentKeempat : Fragment() {

    private var _binding: FragmentNavKeempatBinding? = null
    private val binding get() = _binding!!
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentNavKeempatBinding.inflate(inflater, container, false)
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.btnToFragmentKetiga.setOnClickListener() {

            if (binding.edit1.text.isNullOrEmpty()||binding.edit2.text.isNullOrEmpty()||binding.edit3.text.isNullOrEmpty()) {
                Toast.makeText(requireContext(), "Kolom masih kosong", Toast.LENGTH_SHORT).show()
            } else {
//                println(binding.edit1.text.toString().toInt())
                val detail = Person(binding.edit1.text.toString().toInt(), binding.edit2.text.toString(), binding.edit3.text.toString())
                findNavController().previousBackStackEntry?.savedStateHandle?.set(EXTRA_DETAIL, detail)
                findNavController().navigateUp()
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
    companion object{
        val EXTRA_DETAIL = "EXTRA_DETAIL"
    }
}