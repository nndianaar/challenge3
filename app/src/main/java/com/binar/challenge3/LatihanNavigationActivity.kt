package com.binar.challenge3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.binar.challenge3.databinding.ActivityLatihanNavigationBinding

class LatihanNavigationActivity : AppCompatActivity() {
    private lateinit var binding : ActivityLatihanNavigationBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLatihanNavigationBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}